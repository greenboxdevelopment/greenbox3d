﻿// Guids.cs
// 
// Copyright (c) 2013 The GreenBox Development LLC, all rights reserved
// 
// This file is a proprietary part of GreenBox3D, disclosing the content
// of this file without the owner consent may lead to legal actions

using System;

namespace GreenBox3D.VisualStudio
{
    internal static class GuidList
    {
        public const string GreenBox3DPackage = "40d6c37c-15b4-41fa-9846-b200b5377f19";
        public const string ContentProjectFactory = "F85F0D86-2497-4640-A1D2-15D060AB6EA5";
    };
}
